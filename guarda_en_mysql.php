<?php	

	require("name_file.php"); // para obtener el nombre del archivo
	require("librerias/PHPMailer/class.phpmailer.php"); //para enviar el mail

	//  LECTURA DEL ARCHIVO CSV ///////////////////////////////////////////////////////
		
	$datos = array();

	// Comprovar que el archivo a procesar existe o parar la ejecución
	if (file_exists($name_file)==false) {
		exit("El archivo $name_file no existe\n");
	}

	$fp = fopen ( $name_file , "r" );
	while (( $data = fgetcsv ( $fp, 1000, "," )) !== FALSE ) { // Mientras hay líneas que leer...

	    $i = 0;
	    foreach($data as $row) {
	        $datos[$i]=$row;
	        $i++ ;
	    }	
	    $filas[] = $datos;
	}
	fclose ( $fp );
	

	// CARGA DE PARMETROS ////////////////////////////////////////////////////////
	if (is_null(@$argv[1])) {
		exit(">>> ERROR: Es necesario establecer un archivo con parametros de configuración para este script\n");
	}
	if (file_exists(@$argv[1])==false) {
		exit(">>> ERROR: Parametros de configuración incorrectos (ruta o archivo incorrecto)\n");
	}

	$xml = simplexml_load_file($argv[1]);
				
	// parametros para base de datos
	$host=$xml->parametro[0]->host;
	$user=$xml->parametro[0]->user;
	$pass=$xml->parametro[0]->pass;
	$bd=$xml->parametro[0]->bd;

	// parametros parala configuracion del correo
	$mailUser=$xml->parametro[1]->mailuser;
	$nombre=$xml->parametro[1]->name;
	$mailPass=$xml->parametro[1]->mailpass;
	$Destinatario=$xml->parametro[1]->destinatario;
	$smtpServer=$xml->parametro[1]->smtpserver;

	//  CONECTAR A LA BASE DE DATOS /////////////////////////////////////////////////

	$mysqli = new mysqli($host, $user, $pass, $bd);
	if ($mysqli->connect_errno) {
	    echo "Fallo al contenctar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}	
	

	//  INSERTAR DATOS LEIDOS EN LA BASE DE DATOS ///////////////////////////////////
	$contador_de_estudiantes=0;
	foreach ($filas as $index) {
			
		$my_sql= "INSERT INTO estudiante (nombre, apellidos, correo, cedula, telefono) 
		VALUES ('$index[0]', '$index[1]', '$index[2]', '$index[3]', '$index[4]')";

		$mysqli->query($my_sql)or die("ERROR: ".$mysqli->error."\n");
		$contador_de_estudiantes++;
	}
	

	//  ENVIAR CORREO DE NOTIFICACION ///////////////////////////////////

	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->SMTPAuth = true;
	$mail->SMTPSecure = "ssl";
	$mail->Host = $smtpServer;
	$mail->Port = 465;
	$mail->Username = $mailUser;
	$mail->Password = $mailPass;
	$mail->From  = $mailUser;
	$mail->FromName = $nombre;
	$mail->Subject = "Carga a base de datos completada";
	$mail->AddAddress($Destinatario);

	date_default_timezone_set("America/Costa_Rica");
	$dia= date("d/m/Y");
	$hora = date("h:i a");

	$cuerpo= "La carga de datos del archivo csv a la base de datos se realizó con éxito el:\n";
	$cuerpo.= "$dia a las $hora\nCantidad de estudiantes procesados: $contador_de_estudiantes\n";
	$mail->Body = $cuerpo;

	if($mail->Send()) {
		echo "Mensaje enviado correctamente\n";
	} else {	
		
		echo "-($hora)- Error al enviar mensaje: " . $mail->ErrorInfo."\n";
	}

	
		
?> 